package concessionaire;


public class Motorcycle extends Vehicle {
    
    private int cc;
    
    public Motorcycle(String brand, String model, double price, int cc){
        super(brand, model, price);
        this.cc = cc;
    }

    /**
     * @return the cc
     */
    public int getCc() {
        return cc;
    }

    /**
     * @param cc the cc to set
     */
    public void setCc(int cc) {
        this.cc = cc;
    }
    
    public void printInformation() {
        String message = "Marca: " + getBrand() + " // Modelo: " +  getModel()+ " // Cilindrada: " + cc + "c // Precio: " + String.format("$ %(,.2f",  getPrice()) ;
        System.out.println(message);
    }
    
    
}
