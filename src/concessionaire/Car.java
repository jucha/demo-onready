
package concessionaire;


public class Car extends Vehicle {
    
    private int doors;
    
    
    public Car(String brand, String model, double price, int doors){
        super(brand, model, price);
        this.doors = doors;
    }

    

    /**
     * @return the doors
     */
    public int getDoors() {
        return doors;
    }

    /**
     * @param doors the doors to set
     */
    public void setDoors(int doors) {
        this.doors = doors;
    }
    
    public void printInformation() {
        String message = "Marca: " + getBrand() + " // Modelo: " +  getModel()+ " // Puertas: " + doors + " // Precio: " + String.format("$ %(,.2f", getPrice()) ;
        System.out.println(message);
    }
     
}
