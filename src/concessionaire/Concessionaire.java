package concessionaire;

import java.util.*;

public class Concessionaire {

    public static void main(String[] args) {

        List<Vehicle> allVehicles = new ArrayList<Vehicle>();
        fillList(allVehicles);

        printAllList(allVehicles);
        
        System.out.println("==============================");
        
        Vehicle maxPrice = Collections.max(allVehicles, Comparator.comparing(vehicle -> vehicle.getPrice()));
        Vehicle minPrice = Collections.min(allVehicles, Comparator.comparing(vehicle -> vehicle.getPrice()));

        System.out.println("Vehículo más caro: " + maxPrice.getBrand() + " " + maxPrice.getModel());
        System.out.println("Vehículo más barato: " + minPrice.getBrand() + " " + minPrice.getModel());
        searchStringInModel("Y", allVehicles);
        
        System.out.println("==============================");
        
        // Ambas alternativas validas.
        // Collections.sort(allVehicles,  Collections.reverseOrder(Comparator.comparing(vehicle -> vehicle.getPrice())));
        Collections.sort(allVehicles, (vehicle1, vehicle2) -> vehicle1.getPrice() < vehicle2.getPrice() ? 1 : -1);
        printAllList(allVehicles);
    }
    
    static void fillList(List<Vehicle> list){
        list.add(new Car("Peugeot", "206", 200000, 4));
        list.add(new Motorcycle("Honda", "Titan", 60000, 125));
        list.add(new Car("Peugeot", "208", 250000, 5));
        list.add(new Motorcycle("Yamaha", "YBR", 80500.50, 160));
    }
    
    static void searchStringInModel(String StringToSearch, List<Vehicle> list) {
    for (Vehicle element : list) {
            if (element.containsSubStringInModel(StringToSearch)) {
                System.out.println("Vehículo que contiene la letra " + "'" + StringToSearch + "' " + element.getBrand() + " " + element.getModel() + " " + String.format("$ %(,.2f", element.getPrice()));
            }
        }
    }

    static void printAllList(List<Vehicle> list) {
        
        list.forEach((object) -> object.printInformation());
        
    }
}
